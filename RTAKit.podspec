Pod::Spec.new do |s|
 s.name         = "RTAKit"
 s.version      = "0.0.6"
 s.summary      = "RTAKit"
 s.description  = <<-DESC
 An extended description of RTAKit project.
 DESC
 s.homepage     = "http://www.tvsmiles.de"
 s.license = { :type => 'Copyright', :text => <<-LICENSE
 Copyright 2019
 Permission is granted to...
 LICENSE
 }
 s.author             = { "$(git config user.name)" => "$(git config user.email)" }
 s.source       =  { :http => "https://rta-kit.s3.eu-central-1.amazonaws.com/RTAKit.zip" }

 s.ios.vendored_frameworks = 'RTAKit.framework'
#s.source_files  = "RTAKit/Sources/**/*.{h,m,swift}"
#s.resources = "RTAKit/Resources/*"
 s.platform = :ios
 s.swift_version = "4.2"
 s.ios.deployment_target  = '9.0'
#s.static_framework = true
#s.public_header_files = "RTAKit/RTAKit.h"


 s.subspec 'Core' do |ss|
   ss.dependency  'SwiftyJSON', '5.0.0'

   ss.dependency 'AdinCube/Core', '2.6.6'
   ss.dependency 'AdinCube/Amazon', '2.6.6'
   ss.dependency 'AdinCube/Tapjoy', '2.6.6'
   ss.dependency 'AdinCube/Chartboost', '2.6.6'
   
#ss.source_files = 'RTAKit/Sources/SDK Adapters/SDKAdapter.swift', 'RTAKit/Sources/*.swift'
   ss.frameworks  = 'SafariServices', 'WebKit'
 end
 
 s.subspec 'FyberSDK' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'FyberSDK', '8.22.0'
 end
 
 s.subspec 'Admob' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'Google-Mobile-Ads-SDK', '7.50.0'
#   ss.source_files = 'RTAKit/Sources/SDK Adapters/AdmobAdapter.swift', 'RTAKit/Sources/SDK Adapters/Rewarded videos/AdmobRVAdapter.swift'
 end

 s.subspec 'Aerserv' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'aerserv-ios-sdk'
# ss.source_files = 'RTAKit/Sources/SDK Adapters/AerservAdapter.swift'
 end
 
 s.subspec 'GoogleIMA' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'GoogleAds-IMA-iOS-SDK', '3.9.0'
#   ss.source_files = 'RTAKit/Sources/SDK Adapters/GoogleIMAVastAdapter.swift', 'RTAKit/Sources/SDK Adapters/GoogleIMAVastAdapter.swift'
 end
 
 s.subspec 'OpenBidSDK' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'OpenBidSDK', '1.2.0'
# ss.source_files = 'RTAKit/Sources/SDK Adapters/OguryAdapter.swift'
 end
 
 s.subspec 'FBAudienceNetwork' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'FBAudienceNetwork', '4.28.1'
# ss.source_files = 'RTAKit/Sources/SDK Adapters/Rewarded videos/FANRewardedVideoAdapter.swift'
 end
 
 s.subspec 'FeedAd' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'FeedAd', '1.1.6'
#   ss.source_files = 'RTAKit/Sources/SDK Adapters/FeedAdAdapter.swift'
 end
 
 s.subspec 'SpotX' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'SpotX-SDK', '4.2.1'
   ss.dependency 'SpotX-SDK-MOAT', '4.2.1'
#   ss.source_files = 'RTAKit/Sources/SDK Adapters/SpotXAdapter.swift'
 end
 
 s.subspec 'IronSource' do |ss|
   ss.dependency 'RTAKit/Core'
   ss.dependency 'IronSourceSDK', '6.8.4'
#  ss.source_files = 'RTAKit/Sources/SDK Adapters/IronsourceAdapter.swift', 'RTAKit/Sources/SDK Adapters/Rewarded videos/IronsourceRVAdapter.swift'
 end
 
 s.subspec 'AAT' do |ss|
   ss.dependency 'RTAKit/Core'
   
   ss.dependency 'AATKit/Core', '2.68.11'
   ss.dependency 'AATKit/Applovin', '2.68.11'
   ss.dependency 'AATKit/Mopub', '2.68.11'
   ss.dependency 'AATKit/Unity', '2.68.11'
   ss.dependency 'AATKit/AdColony', '2.68.11'
   ss.dependency 'AATKit/SmartAdServer', '2.68.11'
   ss.dependency 'AATKit/Smaato', '2.68.11'
   ss.dependency 'AATKit/Inmobi', '2.68.11'
   ss.dependency 'AATKit/Vungle', '2.68.11'
   
# ss.source_files = 'RTAKit/Sources/SDK Adapters/AddapptrAdapter.swift'
 end

end
